;(function (angular) {

  angular.module('app', [])
    .controller('MainController', MainController)
  ;


  function MainController() {
    var vm = this;

    vm.chosen = {};

    vm.updateFromNewChoices = updateFromNewChoices;

    // Just here so the sample data below can refer to it
    //
    vm.searchModel = {
      compFilter: {
        vdnMatch:    true,
        poeMatch:    false,
        podMatch:    false,
        cubeMatch:   false,
        weightMatch: false
      }
    };

    // Original sample data
    //
    vm.lobProblems = {
      vdn: { model: vm.searchModel.compFilter.vdnMatch, label: 'VDN' },
      poe: { model: vm.searchModel.compFilter.poeMatch, label: 'POE' },
      pod: { model: vm.searchModel.compFilter.podMatch, label: 'POD' },
      dim: {
        model: (vm.searchModel.compFilter.cubeMatch
            || vm.searchModel.compFilter.weightMatch),
        label: 'DIM'
      }
    };


    // Initialization

    // Preload vm.chosen based on sample data.
    //
    updateChosenFromLobProblems();


    // Functions

    // Utility function to (re)load vm.chosen from vm.lobProblems.
    //
    function updateChosenFromLobProblems() {
      for (var item in vm.lobProblems) {
        if (vm.lobProblems.hasOwnProperty(item)) {
          vm.chosen[item] = vm.lobProblems[item].model;
        }
      }
    }


    // Called whenever the user clicks a button in the button bar,
    // to keep the complex data structure, vm.lobProblems, up to date,
    // and to reset vm.chosen based on the new choices.
    //
    function updateFromNewChoices(chosen) {

      for (var item in vm.lobProblems) {
        if (vm.lobProblems.hasOwnProperty(item)) {
          vm.lobProblems[item].model = !!chosen[item]; // coerce to boolean
        }
      }

      // Assign vm.chosen a new reference, so the change will be detected
      // (via $onChanges) inside instances of buttonBar. Just changing
      // properties of vm.chosen won't be detected.
      //
      vm.chosen = {};
      
      // Keep vm.chosen in sync with vm.lobProblems.
      //
      updateChosenFromLobProblems();
    }
    
  }

})(angular);

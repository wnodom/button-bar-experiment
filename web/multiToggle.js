(function (module) {
  module.component('multiToggle', {
    templateUrl: 'multiToggle.html',
    bindings: {
      choices: '<',
      mode: '@'
    },
    controller: MultiToggleController
  });

  function MultiToggleController() {
    var $ctrl = this;
    $ctrl.toggle = toggle;

    function toggle(item) {
      item.model = !item.model;
      if ($ctrl.mode === 'radio') {
        Object.keys($ctrl.choices).forEach(function (key) {
          if ($ctrl.choices[key].label !== item.label) {
            $ctrl.choices[key].model = false;
          }
        })
      }
      console.log(item.model);
    }
  }
})( angular.module('app') );

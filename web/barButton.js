(function (module) {

  // Original .component version, without the experimental
  // transcluded automatic value. This is a lot simpler than the
  // .directive version, but doesn't allow access to the link
  // function, which is necessary to get to the transclude
  // function.
  //
  // module.component('barButton', {
  //   templateUrl: 'barButton.html',
  //   bindings: {
  //     value: '@'
  //   },
  //   transclude: true,
  //   require: {
  //     buttonBarCtrl: '^buttonBar'
  //   },
  // })

  module.directive('barButton', function () {
    return {
      restrict: 'E',
      templateUrl: 'barButton.html',
      controller: function () {},
      controllerAs: '$ctrl',
      scope: {},
      bindToController: {
        value: '@'
      },
      transclude: true,
      require: {
        barButtonCtrl: 'barButton',
        buttonBarCtrl: '^buttonBar'
      },
      link: linkFn
    };
  });

  function linkFn(scope, elem, attrs, ctrls, transclude) {

    // If no value attribute was provided, use the (trimmed,
    // lowercased) transcluded text as the value.
    //
    if (!ctrls.barButtonCtrl.value) {
      transclude(scope, function (clone) {
        ctrls.barButtonCtrl.value =
            clone[0].innerText.trim().toLowerCase();
      });
    }

  }

})( angular.module('app') );

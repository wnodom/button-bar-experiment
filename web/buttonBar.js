(function (module) {

  module.component('buttonBar', {
    templateUrl: 'buttonBar.html',
    bindings: {
      chosen:   '<',  // one-way binding
      mode:     '@',
      onUpdate: '&'
    },
    controller: ButtonBarController,
    transclude: true
  });


  function ButtonBarController() {
    var $ctrl = this;

    $ctrl.toggle = toggle;
    $ctrl.$onChanges = onChanges; // lifecycle hook

    function onChanges(changesObj) {

      // Make a local copy of the one-way-bound `chosen` binding,
      // to keep from changing it even accidentally.
      //
      // The consumer of this component instance will be notified
      // of any changes via the `onUpdate` handler (if one is
      // specified).
      //
      $ctrl._chosen = Object.assign({}, $ctrl.chosen);
    }

    function toggle(item) {

      // Keep the original value corresponding to the item,
      // since all the existing values might be changed.
      //
      var originalItemValue = $ctrl._chosen[item];

      // In radio mode, set all existing values to false.
      //
      if ($ctrl.mode === 'radio') {
        setAllProperties($ctrl._chosen, false);
      }

      // Toggle the value of the selected item.
      //
      $ctrl._chosen[item] = !originalItemValue;

      // If an `onUpdate` handler was specified, call it to
      // notify the consumer of the updated choices.
      //
      if ($ctrl.onUpdate) {
        $ctrl.onUpdate({ $event: $ctrl._chosen });
      }
    }
  }


  // Utility function to set all properties of the specified
  // object to the specified value.
  //
  function setAllProperties(obj, value) {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        obj[prop] = value;
      }
    }
  }

})( angular.module('app') );
